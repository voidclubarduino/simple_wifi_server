/*
  Webserver Simples, permite controlar um Led
  criado a 28 Abril 2016
  por Ivo Oliveira e Bruno Horta
 */
/**
 * INCLUIR A Biblioteca do ESP
 */
#include <ESP8266WiFi.h>
 
const char* ssid = "TESLA";
const char* password = "xptoxpto";
 
int ledPin = 2; // D4, para mais info ler o Pinout do NODE MCU
//Instancia o WebServer no porto 80
WiFiServer server(80);

void setup() {
  //Incia a comunicação Serie para DEBUG
  Serial.begin(115200);
  //Protege de overhead
  delay(10);
  
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  
  Serial.print("A conectar a ");
  Serial.println(ssid);

  //Inicia o Wi-Fi
  WiFi.begin(ssid, password);
  //Aguarda até estar connectado
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  //DEBUG
  Serial.println("");
  Serial.println("WiFi ligado");
  
  //Inicia o Serviço HTTP
  server.begin();
  //DEBUG
  Serial.println("Servidor iniciado");
  Serial.print("URL para acesso no browser: ");
  Serial.print("http://");
  //Imprime o IP do Modulo
  Serial.print(WiFi.localIP());
  Serial.println("/");
}
 
void loop() {
  //Verificar se algum cliente se ligou
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
 
  //Esperar por pedidos do cliente
  while(!client.available()){
    delay(1);
  }
 
  // Ler a primeira linha do pedido
  String request = client.readStringUntil('\r');
  Serial.println(request);
  //Descartar bytes que foram escritos para o cliente, mas ainda não lidos.
  client.flush();
 

  int value = LOW;
  //Analiza o caminho URL
  if (request.indexOf("/state/on") != -1)  {
    digitalWrite(ledPin, HIGH);
    value = HIGH;
  }
  if (request.indexOf("/state/off") != -1)  {
    digitalWrite(ledPin, LOW);
    value = LOW;
  }
  //Processo o comando para o LED
  digitalWrite(ledPin, value);
 
  // Returna a resposta, exemplo formato HTML
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("");
  client.println("<!DOCTYPE HTML>");
  client.println("<html>"); 
  client.print("O Led está agora ");

  if(value == HIGH) {
    client.print("ligado");
  } else {
    client.print("desligado");
  }
  client.println("</html>"); 
}
 
